# Numpy Neural Network

The goal was to create neural network from scratch to get better understanding of them. The code is written to see all the steps clearly and not be the sortest.

Sigmoid is used because it was used in the math in the original course. ReLU is more common nowadays. I haven't implemented all the bells and whistles possible to keep the code simple and make it easy to understand it.

My notes for course [Neural Networks and Deep Learning](https://www.notion.so/lankinen/Neural-Networks-and-Deep-Learning-aed87fafc0d745369c1f14fbbabb4815) by Andrew Ng